/**
 * @author Nicholas Pritchard 2017/2018
 */
#include <getopt.h>
#include <sys/param.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <gsl/gsl_vector.h>
#include <stdbool.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_spmatrix.h>
#include <gsl/gsl_sf.h>
#include <petsc.h>
#include <slepc.h>

#define PROBABILTY 0.5
#define GRAPHSIZE 4
#define RANDOMITERATE 128
#define MAXIMUMITERATE 512
#define ALPHA 1.0   //Reflection coefficient
#define BETA 0.5   //Contraction coefficient
#define GAMMA 2.0   //Expansion coefficient
bool verbose = false;
bool report = false;
bool automatic = false;
FILE *fp[24];
FILE *fout[24];
struct timeval startTime[24], endTime[24], privateTime[24];
gsl_matrix_int *graphA;
gsl_matrix_int *graphB;
Mat petOperatorC;
Mat petOperatorB;

//------------------------------Function Declarations---------------------------->
gsl_matrix_int* GSL_genGraphMatrix(gsl_rng* rng, size_t graphSize, float probability);
gsl_matrix_int* GSL_genIsoGraph(gsl_matrix_int* graphA, const size_t graphSize);
gsl_matrix_int* GSL_getGraphMatrix(FILE* fp, size_t graphSize);
size_t genHamiltonian(gsl_matrix_int* graphA, gsl_matrix_int* graphB, gsl_permutation* gbMap, size_t graphSize);
gsl_spmatrix* genOperatorC(gsl_matrix_int* graphA, gsl_matrix_int* graphB, size_t graphSize, int rank);
gsl_spmatrix* kroneckerProductDesctSP(gsl_spmatrix* mat1, gsl_spmatrix* mat2);
gsl_spmatrix* genOperatorB(size_t graphSize);
Mat getOperatorB(int graphSize);
Vec setInitialState(size_t graphSize);
Vec applyExpo(PetscScalar delta, Mat operator, Vec state);
Vec genState(PetscScalar gamma, PetscScalar beta, Mat operatorC, Mat operatorB, Vec state);
Vec optimise(PetscInt p, size_t graphSize, PetscMPIInt rank);
Vec probabilities(PetscScalar* parameters, int p, size_t graphSize, int rank);
void graphSim(gsl_matrix_int* graphA, gsl_matrix_int* graphB, size_t graphSize, const int p, bool nelder);
void testRun(bool useFile, char* filename, size_t graphSize, bool isomorphic, bool delete, float probability, int p, bool nelder);
PetscScalar* nelderMead(double start[], int n, int p, double EPSILON, double scale, const size_t graphSize, int rank);

/**
 * Called in error
 */
void printUsage(){
    printf("Please use correctly: -v: verbose; -s: size -r: report\n");
}

/**
 * Called to set the start of some event
 * @param startTime
 */
void reportStart(struct timeval* startTime){
    gettimeofday(startTime, NULL);
}

/**
 * Called to report the finishing of an event
 * @param endTime
 * @param startTime
 * @param msg A string which decribes the event which has finished
 */
void reportEnd(struct timeval* endTime, struct timeval* startTime, char* msg, int rank){
    gettimeofday(endTime,NULL);
    double duration = ((endTime->tv_sec  - startTime->tv_sec) * 1000000u + endTime->tv_usec - startTime->tv_usec) / 1.e6;
    PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%s %12.6fs \n",msg,duration);
    //printf("%s %12.10fs \n",msg,duration);
}

void newFile(bool useFile, char* filename, size_t graphSize, bool isomorphic, bool delete, float probability, int p, bool nelder, time_t* time, int iter, int rank){
    char* newFilename = malloc(sizeof(char)*256);
    newFilename[0] = '\0';
    char argument[128];
    struct tm *timenow;
    timenow = gmtime(time);
    argument[0]='\0';
    sprintf(argument,"%ld",graphSize);
    strcat(newFilename,argument);
    if(useFile){
        strcat(newFilename,filename);
    } else {
        if(isomorphic){
            strcat(newFilename,"_iso");
        }
        if(delete){
            strcat(newFilename,"_del");
        }
    }
    argument[0] = '\0';
    strftime(argument,sizeof(argument),"_%d_%m__%y_%H_%M__%S",timenow);
    strcat(newFilename,argument);
    sprintf(argument,"_%d",iter);
    strcat(newFilename,argument);
    strcat(newFilename,".graph");
    printf(newFilename);
    printf("\n");
    PetscFOpen(PETSC_COMM_WORLD,newFilename,"w+",&fout[rank]);
    free(newFilename);
}


PetscErrorCode writeOperatorB(int rank){
    PetscErrorCode ierr;
    struct timeval startTime, endTime;
    PetscViewer myview;
    for (int i = 9; i <= 10; ++i) {
        reportStart(&startTime);
        gsl_spmatrix* operatorB = genOperatorB((size_t)i);
        Mat petOperatorB;
        char* name = malloc(sizeof(char)*(12+i));
        sprintf(name,"operatorB%d.gz",i);
        ierr = MatCreate(PETSC_COMM_WORLD,&petOperatorB);CHKERRQ(ierr);
        ierr = MatSetSizes(petOperatorB,PETSC_DECIDE,PETSC_DECIDE,(PetscInt)operatorB->size1,(PetscInt)operatorB->size2);CHKERRQ(ierr);
        ierr = MatSetFromOptions(petOperatorB);CHKERRQ(ierr);
        ierr = MatSetUp(petOperatorB);CHKERRQ(ierr);
        for (int j = 0; j < operatorB->nz; ++j) {
            MatSetValue(petOperatorB,(PetscInt)operatorB->i[j],(PetscInt)operatorB->p[j],operatorB->data[j],INSERT_VALUES);
        }
        ierr = MatAssemblyBegin(petOperatorB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
        ierr = MatAssemblyEnd(petOperatorB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
        gsl_spmatrix_free(operatorB);
        ierr = PetscViewerBinaryOpen(PETSC_COMM_SELF,name,FILE_MODE_WRITE,&myview);CHKERRQ(ierr);
        ierr = MatView(petOperatorB,myview);CHKERRQ(ierr);
        ierr = PetscViewerDestroy(&myview);CHKERRQ(ierr);
        ierr = MatDestroy(&petOperatorB);CHKERRQ(ierr);
        char* msg = malloc(sizeof(char)*(24+i));
        sprintf(msg,"Finished processing %d in ",i);
        reportEnd(&endTime,&startTime,msg,rank);
        free(msg);
        free(name);
    }
}

void isolatedTest(int rank){
    writeOperatorB(rank);
}

int main(int argc, char** argv) {
    int p = 1;
    time_t now;
    size_t graphSize = GRAPHSIZE;
    float probability = PROBABILTY;
    //------------------------------Petsc Boilerplate-------------------------------->
    static char help[] = "Implements the QAOA algorithm for graph similarity";
    char* filename = "notFile";
    bool useFile = false;
    bool nelder = false;
    bool isomorphic = false;
    bool delete = false;
    PetscMPIInt rank;
    int itr = 1;
    PetscErrorCode ierr;
    ierr = SlepcInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    //---------------------Parse Arguments--------------------------------------->
    int opt;
    while ((opt = getopt(argc, argv, "s:vrtp:f:idanm:")) != -1) {
        switch (opt) {
            case 's':
                graphSize = (unsigned int)atoi(optarg);
                if(graphSize == 0){
                    printUsage();
                    exit(EXIT_FAILURE);
                }
                break;
            case 'v':
                verbose = true;
                break;
            case 'r':
                report = true;
                break;
            case 'p':
                p = atoi(optarg);
                if(p==0){
                    printUsage();
                    exit(EXIT_FAILURE);
                }
                break;
            case 't': //An argument for bypassing the main program and testing a particular feature
                newFile(useFile,filename,graphSize,isomorphic,delete,probability,p,nelder,&now,1, rank);
                //isolatedTest();
                return 0;
            case 'f':
                filename = optarg;
                useFile = true;
                break;
            case 'i':
                isomorphic = true;
                break;
            case 'd':
                delete = true;
                break;
            case 'a':
                automatic = true;
                break;
            case 'n':
                nelder = true;
                break;
            case 'm':
                itr = atoi(optarg);
                if(itr==0){
                    itr=1;
                }
                break;
            default:
                printUsage();
                exit(EXIT_FAILURE);
        }
    }
    for (int i = 0; i < itr; ++i) {
        time(&now);
        if(report&&rank==i){
            newFile(useFile,filename,graphSize,isomorphic,delete,probability,p,nelder,&now,i+1,rank);
        }
        testRun(useFile,filename,graphSize, isomorphic, delete, probability,p,nelder);
        PetscPrintf(PETSC_COMM_WORLD,"Test run finished\n");
        if(report&rank==i){
            fclose(fout[i]);
        }
    }
    printf("\n");
    SlepcFinalize();
    return 0;
}

void testRun(bool useFile, char* filename, size_t graphSize, bool isomorphic, bool delete, float probability, int p, bool nelder) {
    PetscErrorCode ierr;
    struct timeval startTime, endTime;
    PetscMPIInt size;
    PetscMPIInt rank;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRABORT(PETSC_COMM_WORLD, ierr);
    if(report){
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"MPISize: %d\n",size);
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"graphSize: %ld\n",graphSize);
        if(!useFile){
            PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"probability: %f\n",probability);
        }
    }
    if (report)
        reportStart(&startTime);
        if(!useFile) {
            gsl_rng_env_setup();
            gsl_rng *rng = gsl_rng_alloc(gsl_rng_mt19937_1999);
            time_t seed;
            time(&seed);
            gsl_rng_set(rng, (size_t) seed+rank);
            graphA = GSL_genGraphMatrix(rng, graphSize, probability);
            if(isomorphic){
                graphB = GSL_genIsoGraph(graphA,graphSize);
            } else {
                graphB = GSL_genGraphMatrix(rng, graphSize, probability);
            }
            if(delete){
                size_t randV1 = 0;
                size_t randV2 = 0;
                while(gsl_matrix_int_get(graphA,randV1,randV2)!=1){
                    randV1 = (gsl_rng_get(rng)%graphSize);
                    randV2 = (gsl_rng_get(rng)%graphSize);
                }
                gsl_matrix_int_set(graphA,randV1,randV2,0);
            }
            gsl_rng_free(rng);
        } else {
            ierr = PetscFOpen(PETSC_COMM_WORLD, filename, "r", &fp[rank]);CHKERRABORT(PETSC_COMM_WORLD, ierr);
            graphA = GSL_getGraphMatrix(fp[rank], graphSize);
            graphB = GSL_getGraphMatrix(fp[rank], graphSize);
            fclose(fp[rank]);
        } //If neither, reuse the old graphs
    if (report){
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"Graph A:\n");
        gsl_matrix_int_fprintf(fout[rank],graphA,"%d");
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"Graph B:\n");
        gsl_matrix_int_fprintf(fout[rank],graphB,"%d");
        reportEnd(&endTime, &startTime, "Graph Gen:", rank);
    }
    graphSim(graphA,graphB,graphSize,p,nelder);
}

//TODO: Make parallel by running multiple tests.
void graphSim(gsl_matrix_int* graphA, gsl_matrix_int* graphB, size_t graphSize, const int p, bool nelder){
    PetscErrorCode ierr;
    PetscMPIInt size;
    PetscMPIInt rank;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    //------------------------------Declare and setup Petsc Data-Structures---------->
    size_t cSize = (size_t)gsl_sf_pow_int(2,(int)ceill(gsl_sf_log(gsl_sf_fact((int)graphSize))/gsl_sf_log(2)));
    ierr = MatCreate(PETSC_COMM_WORLD,&petOperatorC);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatSetSizes(petOperatorC,PETSC_DECIDE,PETSC_DECIDE,(PetscInt)cSize,(PetscInt)cSize);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatSetFromOptions(petOperatorC);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatSetUp(petOperatorC);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    //------------------------------Generate two graphs randomly--------------------->
        //------------------------------Build Operator C--------------------------------->
        //Define the Hamiltonian
    if (report){
        reportStart(&startTime[rank]);
    }
    gsl_spmatrix *operatorC = genOperatorC(graphA, graphB, graphSize, rank);
    for (int i = 0; i < operatorC->nz; ++i) {
        ierr = MatSetValue(petOperatorC, (PetscInt) operatorC->i[i], (PetscInt) operatorC->p[i], operatorC->data[i], INSERT_VALUES);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
    gsl_spmatrix_free(operatorC);
    if (report){
        reportEnd(&endTime[rank], &startTime[rank], "Operator C Gen:",rank);
    }
    //Assemble petsc OperatorC
    ierr = MatAssemblyBegin(petOperatorC, MAT_FINAL_ASSEMBLY);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatAssemblyEnd(petOperatorC, MAT_FINAL_ASSEMBLY);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if (verbose) {
        ierr = MatView(petOperatorC, PETSC_VIEWER_STDOUT_WORLD);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
    //------------------------------Build Operator B--------------------------------->
    ierr = MatCreate(PETSC_COMM_WORLD,&petOperatorB);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatSetSizes(petOperatorB,PETSC_DECIDE,PETSC_DECIDE,(PetscInt)cSize,(PetscInt)cSize);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatSetFromOptions(petOperatorB);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatSetUp(petOperatorB);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if (report) {
        reportStart(&startTime[rank]);
    }
    gsl_spmatrix *operatorB = genOperatorB(graphSize);
    for (int i = 0; i < operatorB->nz; ++i) {
        ierr = MatSetValue(petOperatorB, (PetscInt) operatorB->i[i], (PetscInt) operatorB->p[i], operatorB->data[i], INSERT_VALUES);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
    gsl_spmatrix_free(operatorB);
    ierr = MatAssemblyBegin(petOperatorB, MAT_FINAL_ASSEMBLY);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MatAssemblyEnd(petOperatorB, MAT_FINAL_ASSEMBLY);CHKERRABORT(PETSC_COMM_WORLD,ierr);
        //petOperatorB = getOperatorB((int)graphSize);
    if(verbose){
        //ierr = MatView(petOperatorB,PETSC_VIEWER_STDOUT_WORLD);CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
    if(report){
        reportEnd(&endTime[rank],&startTime[rank], "Operator B Gen:",rank);
    }
    //------------------------------Optimisation-------------------------------------->
    if (report) {
        reportStart(&startTime[rank]);
    }
    if (automatic) {
        for (int i = 1; i < 7; ++i) {
            double *start = malloc(sizeof(double) * 2 * i);
            for (int j = 0; j < i; ++j) {
                start[j] = 2 * M_PI;
                start[j + i] = M_PI;
            }
            if (report) {
                PetscFPrintf(PETSC_COMM_WORLD, fout[rank], "Nelder-Mead %d\n", i);
            }
            PetscScalar *params = nelderMead(start, 2 * i, i, 1e-3, 1.0, graphSize, rank);
            Vec result = probabilities(params, i, graphSize, rank);
            VecDestroy(&result);
            free(start);
            PetscPrintf(PETSC_COMM_WORLD, "Finished Nelder-Mead %d\n", i);
        }
        for (int i = 1; i < 7; ++i) {
            if (report) {
                PetscFPrintf(PETSC_COMM_WORLD, fout[rank], "Monte-Carlo %d\n", i);
            }
            Vec params;
            params = optimise(i, graphSize, rank);
            PetscScalar *vals;
            ierr = VecGetArray(params, &vals);
            CHKERRABORT(PETSC_COMM_WORLD, ierr);
            Vec result = probabilities(vals, p, graphSize, rank);
            VecDestroy(&params);
            VecDestroy(&result);
            PetscPrintf(PETSC_COMM_WORLD, "Finished Monte-Carlo %d\n", i);
        }
    } else {
        if (nelder) {
            double *start = malloc(sizeof(double) * 2 * p);
            for (int i = 0; i < p; ++i) {
                start[i] = 2 * M_PI;
                start[i + p] = M_PI;
            }
            if (report) {
                PetscFPrintf(PETSC_COMM_WORLD, fout[rank], "Nelder-Mead %d\n", p);
            }
            PetscScalar *params = nelderMead(start, 2 * p, p, 1e-3, 1.0, graphSize, rank);
            Vec result = probabilities(params, p, graphSize, rank);
            VecDestroy(&result);
            free(start);
        } else {
            if (report && rank == 0) {
                PetscFPrintf(PETSC_COMM_WORLD, fout[rank], "Monte-Carlo %d\n", p);
            }
            Vec params;
            params = optimise(p, graphSize, rank);
            PetscScalar *vals;
            ierr = VecGetArray(params, &vals);CHKERRABORT(PETSC_COMM_WORLD, ierr);
            Vec result = probabilities(vals, p, graphSize, rank);
            VecDestroy(&result);
            VecDestroy(&params);
            if (report) {
                char *msg = malloc(sizeof(char) * 128);
                msg[0] = '\0';
                sprintf(msg, "Monte-Carlo %d", p);
                reportEnd(&endTime[rank], &privateTime[rank], msg, rank);
                free(msg);
            }
        }
    }
    if (report) {
        reportEnd(&endTime[rank], &startTime[rank], "Optimisation:", rank);
    }
    MatDestroy(&petOperatorB);
    MatDestroy(&petOperatorC);
}

gsl_matrix_int* GSL_genGraphMatrix(gsl_rng* rng, const size_t graphSize, float probability){
    gsl_matrix_int* adjMatrix = gsl_matrix_int_calloc(graphSize,graphSize);
    double temp;
    for (size_t i = 0; i < graphSize; ++i) {
        for (size_t j = 0; j < graphSize; ++j) {
            temp = gsl_rng_uniform(rng);
            if(temp < probability){
                gsl_matrix_int_set(adjMatrix,i,j,1);
                gsl_matrix_int_set(adjMatrix,j,i,1);
            }
        }
    }
    if(verbose){
        for (size_t i = 0; i < graphSize; ++i) {
            for (size_t j = 0; j < graphSize; ++j) {
                printf("%d",gsl_matrix_int_get(adjMatrix,i,j));
            }
            printf("\n");
        }
        printf("\n");
    }
    return adjMatrix;
}

gsl_matrix_int* GSL_genIsoGraph(gsl_matrix_int* graphA, const size_t graphSize){
    gsl_permutation* gbMap = gsl_permutation_calloc(graphSize);
    gsl_matrix_int* graphB = gsl_matrix_int_calloc(graphSize,graphSize);
    size_t ai, aj;
    size_t bi, bj;
    double sizePermute = gsl_sf_fact((int)graphSize);
    time_t seed;
    time(&seed);
    srand(seed);
    int permutation = (rand()%((int)sizePermute+1));
    for (int i = 0; i < permutation; ++i) {
        gsl_permutation_next(gbMap);
    }
    for (size_t i = 0; i < graphSize; ++i) {
        for (size_t j = 0; j < graphSize; ++j) {
            ai = i;
            aj = j;
            bi = gsl_permutation_get(gbMap,i);
            bj = gsl_permutation_get(gbMap,j);
            if(gsl_matrix_int_get(graphA,ai,aj)==1){
                gsl_matrix_int_set(graphB,bi,bj,1);
            }
        }
    }
    return graphB;
}

gsl_matrix_int* GSL_getGraphMatrix(FILE* fp, size_t graphSize){
    char curr;
    gsl_matrix_int* adjMatrix = gsl_matrix_int_calloc(graphSize,graphSize);
    for (int i = 0; i < graphSize; ++i) {
        for (int j = 0; j < graphSize+1; ++j) {
            fscanf(fp,"%c",&curr);
            if(curr!='\n'){
                if(verbose){
                    printf("%c",curr);
                }
                if(curr=='1'){
                    gsl_matrix_int_set(adjMatrix,(size_t)i,(size_t)j,1);
                }
            }
        }
        if(verbose){
            printf("\n");
        }
    }
    return adjMatrix;
}

size_t genHamiltonian(gsl_matrix_int* graphA, gsl_matrix_int* graphB, gsl_permutation* gbMap, const size_t graphSize){
    size_t ans = 0;
    size_t ai,aj;
    size_t bi,bj;
    for (size_t i = 0; i < graphSize; ++i) { //Vertex in A
        for (size_t j = 0; j < graphSize; ++j) { //Vertex in B - to be mapped
            ai = i;
            aj = j;
            bi = gsl_permutation_get(gbMap,i);
            bj = gsl_permutation_get(gbMap,j);
            /* Only add back in when testing small graphs
            if(verbose)
                printf("ai = %ld, aj = %ld\nbi = %ld, bj = %ld\n",ai,aj,bi,bj);
                */
            if(gsl_matrix_int_get(graphA,ai,aj)!=gsl_matrix_int_get(graphB,bi,bj)){
                ans++;
            }
        }
    }
    if(verbose){
        printf("%ld\n",ans);
    }
    return ans;
}

gsl_spmatrix* genOperatorC(gsl_matrix_int* graphA, gsl_matrix_int* graphB, const size_t graphSize, int rank){
    gsl_permutation* gbMap = gsl_permutation_calloc(graphSize);
    size_t i = 0;
    size_t cSize;
    size_t hamilton;
    gsl_spmatrix* operatorC;
    //Graphs of size 17 will need 49 qubits. A simulation this size would exhaust the most powerful super-computers in the world.
    cSize = (size_t)gsl_sf_pow_int(2,(int)ceill(gsl_sf_log(gsl_sf_fact((int)graphSize))/gsl_sf_log(2)));
    operatorC = gsl_spmatrix_alloc_nzmax(cSize,cSize,cSize,GSL_SPMATRIX_TRIPLET);
    do{
        //normalHam = 1.0-genHamiltonian(graphA,graphB,gbMap,graphSize)/normalBase;
        hamilton = genHamiltonian(graphA,graphB,gbMap,graphSize);
        if(report)PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%ld ",hamilton);
        gsl_spmatrix_set(operatorC,i,i,hamilton);
        ++i;
    }while(gsl_permutation_next(gbMap)==GSL_SUCCESS);
    for (size_t j = (size_t)gsl_sf_fact((int)graphSize); j < cSize; ++j) {
        gsl_spmatrix_set(operatorC,j,j,(double)(graphSize*graphSize-graphSize));
        if(report)PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%ld ",(graphSize*graphSize-graphSize));
    }
    if(report)PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"\n");
    return operatorC;
}

gsl_spmatrix* kroneckerProductDesctSP(gsl_spmatrix* mat1, gsl_spmatrix* mat2){
    gsl_spmatrix* result = gsl_spmatrix_alloc_nzmax(mat1->size1*mat2->size1,mat1->size1*mat2->size1,mat1->nz*2,GSL_SPMATRIX_TRIPLET);
    size_t startRow;
    size_t startCol;
    for (size_t i = 0; i < mat1->size1; ++i) {
        for (size_t j = 0; j < mat1->size2; ++j) {
            startRow = i * mat2->size1;
            startCol = j * mat2->size2;
            if(gsl_spmatrix_get(mat1,i,j)==0){
                continue;
            }
            for (size_t k = 0; k < mat2->size1; ++k) {
                for (size_t l = 0; l < mat2->size2; ++l) {
                    if(gsl_spmatrix_get(mat2,k,l)==0){
                        continue;
                    }
                    gsl_spmatrix_set(result,startRow+k,startCol+l,gsl_spmatrix_get(mat1,i,j)*gsl_spmatrix_get(mat2,k,l));
                }
            }
        }
    }
    gsl_spmatrix_free(mat1);
    return result;
}

gsl_spmatrix* genOperatorB(size_t graphSize){
    size_t opSize = (size_t)ceill(gsl_sf_log(gsl_sf_fact((int)graphSize))/gsl_sf_log(2));
    gsl_spmatrix* normalPauli = gsl_spmatrix_alloc_nzmax(2,2,2,GSL_SPMATRIX_TRIPLET);
    gsl_spmatrix* rotatePauli = gsl_spmatrix_alloc_nzmax(2,2,2,GSL_SPMATRIX_TRIPLET);
    gsl_spmatrix_set(normalPauli,0,0,1);
    gsl_spmatrix_set(normalPauli,1,1,1);
    gsl_spmatrix_set(rotatePauli,0,1,1);
    gsl_spmatrix_set(rotatePauli,1,0,1);
    //Generate Operator B
    gsl_spmatrix* operatorB = gsl_spmatrix_alloc_nzmax((size_t)pow(2,opSize),(size_t)pow(2,opSize),(size_t)(pow(2,opSize)*0.01),GSL_SPMATRIX_TRIPLET);
    for (size_t i = 0; i < opSize; ++i) {
        gsl_spmatrix* result = gsl_spmatrix_alloc(2,2);
        if(i!=0){ //Normie Pauli
            gsl_spmatrix_set(result,0,0,1);
            gsl_spmatrix_set(result,1,1,1);
        } else { //Rotate Pauli
            gsl_spmatrix_set(result,0,1,1);
            gsl_spmatrix_set(result,1,0,1);
        }
        for (size_t j = 1; j < opSize; ++j) {
            if(i!=j){
                result = kroneckerProductDesctSP(result,normalPauli);
            } else {
                result = kroneckerProductDesctSP(result,rotatePauli);
            }
        }
        for (size_t k = 0; k < operatorB->size1; ++k) {
            for (size_t j = 0; j < operatorB->size1; ++j) {
                if(gsl_spmatrix_get(result,k,j)==1){
                    gsl_spmatrix_set(operatorB,k,j,1);
                }
            }
        }
        gsl_spmatrix_free(result);
    }
    if(verbose){
        for (int k = 0; k < operatorB->size1; ++k) {
            for (int j = 0; j < operatorB->size2; ++j) {
                printf("%.0f",gsl_spmatrix_get(operatorB,(size_t)k,(size_t)j));
            }
            printf("\n");
        }
    }
    return operatorB;
}

Mat getOperatorB(const int graphSize){
    Mat output;
    PetscViewer viewer;
    PetscInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    char* name = malloc(sizeof(char)*(12+graphSize));
    sprintf(name,"operatorB%d.gz",graphSize);
    if(report){
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%s\n",name);
    }
    if(rank==0){
        PetscViewerBinaryOpen(PETSC_COMM_SELF,name,FILE_MODE_READ,&viewer);
    }
    MatCreate(PETSC_COMM_WORLD,&output);
    if(rank==0){
        MatLoad(output,viewer);
    }
    PetscViewerDestroy(&viewer);
    if(verbose){
       // MatView(output,PETSC_VIEWER_STDOUT_WORLD);
    }
    free(name);
    return output;
}

Vec setInitialState(const size_t graphSize){
    PetscErrorCode ierr;
    PetscInt opSize = (PetscInt)gsl_sf_pow_int(2,(int)ceill(gsl_sf_log(gsl_sf_fact((int)graphSize))/gsl_sf_log(2)));
    Vec state;
    ierr = VecCreate(PETSC_COMM_WORLD,&state);          CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecSetSizes(state,PETSC_DECIDE,opSize);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecSetFromOptions(state);                    CHKERRABORT(PETSC_COMM_WORLD,ierr);
    PetscInt initIndices[opSize];
    PetscScalar initValues[opSize];
    for (PetscInt i = 0; i < opSize; ++i) {
        initIndices[i]=i;
        initValues[i] = 1/sqrt(opSize) + 0*PETSC_i;
    }
    ierr = VecSetValues(state,opSize,initIndices,initValues,INSERT_VALUES); CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecAssemblyBegin(state);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecAssemblyEnd(state);                       CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if(verbose){
       // VecView(state,PETSC_VIEWER_STDOUT_WORLD);
    }
    return state;
}

Vec applyExpo(PetscScalar delta, Mat operator, Vec state){
    PetscErrorCode ierr;
    MFN mfn;
    FN f;
    Vec y;
    PetscInt size;
    ierr = VecGetSize(state,&size);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&y);              CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecSetSizes(y,PETSC_DECIDE,size);            CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecSetFromOptions(y);                        CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MFNCreate(PETSC_COMM_WORLD,&mfn);            CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MFNSetOperator(mfn,operator);                CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MFNGetFN(mfn,&f);                            CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = FNSetType(f,FNEXP);                          CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = FNSetScale(f,delta*(-1*PETSC_i),1.0);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MFNSetFromOptions(mfn);                      CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MFNSolve(mfn,state,y);                       CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = MFNDestroy(&mfn);                            CHKERRABORT(PETSC_COMM_WORLD,ierr);
    return y;
}

Vec genState(PetscScalar gamma, PetscScalar beta, Mat operatorC, Mat operatorB, Vec state){
    PetscErrorCode ierr;
    Vec result = applyExpo(gamma,operatorC,state);
    Vec result2 = applyExpo(beta,operatorB,result);
    ierr = VecDestroy(&result);                         CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if(verbose){
       // VecView(result2,PETSC_VIEWER_STDOUT_WORLD);
    }
    ierr = VecDestroy(&state);                          CHKERRABORT(PETSC_COMM_WORLD,ierr);
    return result2;
}

PetscErrorCode myFunction(Vec x, PetscReal* f, const int p, size_t graphSize){
    PetscErrorCode ierr;
    Vec initial = setInitialState(graphSize);
    PetscScalar* vals;
    Vec result;
    PetscScalar val;
    ierr = VecGetArray(x,&vals);                        CHKERRQ(ierr);
    for (int i = 0; i < p; ++i) {
        initial = genState(vals[i],vals[i+p],petOperatorC,petOperatorB,initial);
    }
    result = setInitialState(graphSize);
    ierr = MatMultHermitianTranspose(petOperatorC,initial,result);  CHKERRQ(ierr);
    ierr = VecDot(result,initial,&val);                 CHKERRQ(ierr);
    ierr = VecRestoreArray(x,&vals);                    CHKERRQ(ierr);
    ierr = VecDestroy(&result);                         CHKERRQ(ierr);
    ierr = VecDestroy(&initial);                        CHKERRQ(ierr);
    *f = PetscRealPart(val);
    return ierr;
}

double myFunctionSimplex(double v[], const int p, size_t graphSize){
    PetscErrorCode ierr;
    Vec initial;
    Vec result;
    PetscScalar val;
    initial = setInitialState(graphSize);
    for (int i = 0; i < p; ++i) {
        initial = genState(v[i],v[i+p],petOperatorC,petOperatorB,initial);
    }
    result = setInitialState(graphSize);
    ierr = MatMultHermitianTranspose(petOperatorC,initial,result);  CHKERRQ(ierr);
    ierr = VecDot(result,initial,&val);                 CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecDestroy(&result);                         CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecDestroy(&initial);                        CHKERRABORT(PETSC_COMM_WORLD,ierr);
    return PetscRealPart(val);
}

Vec optimise(const PetscInt p, size_t graphSize, PetscMPIInt rank){
    char* message = malloc(sizeof(char)*512);
    if(rank==0)reportStart(&privateTime[rank]);
    message[0]='\0';
    PetscErrorCode ierr;
    Vec soln;
    PetscReal gamma;
    PetscReal beta;
    PetscReal expectedValue;
    PetscRandom randy;
    Vec bestSoln;
    PetscReal worstValue = -PETSC_INFINITY;
    PetscReal bestValue = PETSC_INFINITY;
    ierr = VecCreate(PETSC_COMM_WORLD,&soln);           CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecSetSizes(soln,PETSC_DECIDE,2*p);          CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecSetFromOptions(soln);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
    time_t seed;
    time(&seed);
    ierr = PetscRandomCreate(PETSC_COMM_SELF,&randy);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = PetscRandomSetType(randy,PETSCRAND48);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = PetscRandomSetSeed(randy,(u_long)seed);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = PetscRandomSeed(randy);                      CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = PetscRandomSetInterval(randy,0.0,PETSC_PI);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecAssemblyBegin(soln);                      CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecAssemblyEnd(soln);                        CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecDuplicate(soln,&bestSoln);                CHKERRABORT(PETSC_COMM_WORLD,ierr);
    for (int i = 0; i < RANDOMITERATE; ++i) {
        for (int j = 0; j < p; ++j) {
            PetscRandomGetValueReal(randy,&gamma);
            PetscRandomGetValueReal(randy,&beta);
            VecSetValue(soln,j,2*gamma,INSERT_VALUES);
            VecSetValue(soln,j+p,beta,INSERT_VALUES);
        }
        ierr = VecAssemblyBegin(soln);                      CHKERRABORT(PETSC_COMM_WORLD,ierr);
        ierr = VecAssemblyEnd(soln);                        CHKERRABORT(PETSC_COMM_WORLD,ierr);
        myFunction(soln, &expectedValue, p, graphSize);
        if(expectedValue>worstValue){
            ierr = VecCopy(soln,bestSoln);              CHKERRABORT(PETSC_COMM_WORLD,ierr);
            worstValue=expectedValue;
            if(rank==0){
                sprintf(message,"W %d %f",i,expectedValue);
                if(report){
                    reportEnd(&endTime[rank],&privateTime[rank],message,rank);
                }
                if(verbose){
                    PetscPrintf(PETSC_COMM_WORLD,message);
                    PetscPrintf(PETSC_COMM_WORLD,"\n");
                }
            }
        }
        if(expectedValue<bestValue){
            bestValue=expectedValue;
            if(rank==0){
                sprintf(message,"B %d %f",i,expectedValue);
                if(report){
                    reportEnd(&endTime[rank],&privateTime[rank],message,rank);
                }
                if(verbose){
                    PetscPrintf(PETSC_COMM_WORLD,message);
                    PetscPrintf(PETSC_COMM_WORLD,"\n");
                }
            }
        }
    }
    if(report && rank == 0){
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"Worst %f\n",worstValue);
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"Best %f\n",bestValue);
    }
    return soln;
}

void initialiseSimplex(double** v, double start[], double scale, int n){
    double pn, qn;
    pn = scale*(sqrt(n+1)-1+n)/(n*sqrt(2));
    qn = scale*(sqrt(n+1)-1)/(n*sqrt(2));

    for (int i = 0; i < n; ++i) {
        v[0][i] = start[i];
    }

    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j < n; ++j) {
            if(i-1 == j){
                v[i][j] = pn + start[j];
            } else {
                v[i][j] = qn;
            }
        }
    }
}


void printSimplex(double **v, double *f, int n, int rank){
    for (int i = 0; i <= n; ++i) {
        for (int j = 0; j < n; ++j) {
            PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%f ",v[i][j]);
        }
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],":%f\n",f[i]);
    }
    reportEnd(&endTime[rank],&privateTime[rank],"Initial Simplex",rank);
}

void printIteration(double **v, double *f, int n, int vs, int rank){
    for (int i = 0; i <= n; ++i) {
        for (int j = 0; j < n; ++j) {
            PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%f ",v[i][j]);
        }
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],":%f\n",f[i]);
    }
    char* msg = malloc(sizeof(char)*128);
    msg[0]='\0';
    sprintf(msg,"B %f",f[vs]);
    reportEnd(&endTime[rank],&privateTime[rank],msg,rank);
    free(msg);
}

int findLarge(double f[], int vg, int n){
    for (int i = 0; i < n+1; ++i) {
        if(f[i]>f[vg]){
            vg = i;
        }
    }
    return vg;
}

int findSmall(double f[], int vs, int n){
    for (int i = 0; i < n+1; ++i) {
        if(f[i] < vs){
            vs = i;
        }
    }
    return vs;
}

int findSmall2(double f[], int vh, int vg, int n){
    for (int i = 0; i < n+1; ++i) {
        if(f[i] > f[vh] && f[i] < f[vg]){
            vh = i;
        }
    }
    return vh;
}

void calcCentroid(double *vm, double **v, int n, int vg){
    double cent;
    for (int i = 0; i < n; ++i) {
        cent = 0.0;
        for (int j = 0; j < n + 1; ++j) {
            if(j!=vg){
                cent+=v[j][i];
            }
        }
        vm[i] = cent/n;
    }
}

void constrain(double v[],int p){
    for (int i = 0; i < p; ++i) {
        if(v[i]>2*M_PI){
            v[i] = 2*M_PI;
        }
        if(v[i]<0){
            v[i] = 0.0;
        }
        if(v[i+p]>M_PI){
            v[i+p] = M_PI;
        }
        if(v[i+p]<0){
            v[i+p] = 0.0;
        }
    }
}

PetscScalar* nelderMead(double start[], int n, int p, double EPSILON, double scale, const size_t graphSize, int rank){
    int vs;
    int vh;
    int vg;
    int count;
    double **v; //Holds vertices of simplex
    double *f;  //Holds function values of simplex
    double fr;  //Value at reflection point
    double fe;  //Value at expansion point
    double fc;  //Value at contraction point
    double *vr; //Reflection vertex
    double *ve; //Expansion vertex
    double *vc; //Contraction vertex
    double *vm; //Centroid vertex
    double min;
    double fsum,favg,s;
    if(report)reportStart(&privateTime[rank]);
    //Memory allocs
    v = (double**) malloc((n+1)*sizeof(double*));
    f = (double*) malloc((n+1)*sizeof(double));
    vr = (double*) malloc(n*sizeof(double));
    ve = (double*) malloc(n* sizeof(double));
    vc = (double*) malloc(n*sizeof(double));
    vm = (double*) malloc(n*sizeof(double));

    //Allocate the colums for the simplex
    for (int i = 0; i < n+1; ++i) {
        v[i] = (double*) malloc(n*sizeof(double));
    }

    //Create the initial simplex
    initialiseSimplex(v,start,scale,n);
    //Constrain
    for (int j = 0; j < n + 1; ++j) {
        constrain(v[j],p);
    }
    //Find initial function values
    for (int j = 0; j < n + 1; ++j) {
        f[j] = myFunctionSimplex(v[j],p,graphSize);
    }
    count=n+1;
    if(report){
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"Initial Simplex: \n");
        printSimplex(v,f,n,rank);
    }
    //Main loop
    for (int i = 0; i < MAXIMUMITERATE; ++i) {
        //Find largest vertex
        vg = findLarge(f, 0, n);
        //Find smallest
        vs = findSmall(f, 0, n);
        //Find second smallest
        vh = findSmall2(f, vs, vg, n);
        //Calculate the centroid
        calcCentroid(vm, v, n, vg);
        //Reflect vg to a new vertex r
        for (int j = 0; j < n; ++j) {
            vr[j] = vm[j] + ALPHA * (vm[j] - v[vg][j]);
        }
        //Constrain if needed
        constrain(vr,p);
        fr = myFunctionSimplex(vr, p, graphSize);
        count++;

        if (fr < f[vh] && fr >= f[vs]) {
            for (int j = 0; j < n; ++j) {
                v[vg][j] = vr[j];
            }
            f[vg] = fr;
        }

        //Investigate a step further
        if (fr < f[vs]) {
            for (int j = 0; j < n; ++j) {
                ve[j] = vm[j] + GAMMA * (vr[j] - vm[j]);
            }
        }
        constrain(ve,p);
        fe = myFunctionSimplex(ve, p, graphSize);
        count++;

        if (fe < fr) {
            for (int j = 0; j < n; ++j) {
                v[vg][j] = ve[j];
            }
            f[vg] = fe;
        } else {
            for (int j = 0; j < n; ++j) {
                v[vg][j] = vr[j];
            }
            f[vg] = fr;
        }
        //Check for contraction
        if (fr >= f[vh]) {
            if (fr < f[vg] && fr >= f[vh]) {
                //Outside contraction
                for (int j = 0; j < n; ++j) {
                    vc[j] = vm[j] + BETA * (vr[j] - vm[j]);
                }
                constrain(vc,p);
                fc = myFunctionSimplex(vc, p, graphSize);
                count++;
            } else {
                //Inside contraction
                for (int j = 0; j < n; ++j) {
                    vc[j] = vm[j] - BETA * (vm[j] - v[vg][j]);
                }
                constrain(vc,p);
                fc = myFunctionSimplex(vc, p, graphSize);
                count++;
            }
            if (fc < f[vg]) {
                for (int j = 0; j < n; ++j) {
                    v[vg][j] = vc[j];
                }
                f[vg] = fc;
            } else {
                //Contraction not successful, halve everything
                for (int j = 0; j < n + 1; ++j) {
                    if (j != vs) {
                        for (int k = 0; k < n; ++k) {
                            v[j][k] = v[vs][k] + (v[j][k] - v[vs][k]) / 2.0;
                        }
                    }
                }
                //Re-evaluate all vertices
                for (int j = 0; j < n + 1; ++j) {
                    f[j] = myFunctionSimplex(v[j], p, graphSize);
                }
                count += n;

                vg = findLarge(f, 0, n);
                vs = findSmall(f, 0, n);
                vh = findSmall2(f, vs, vg, n);
                constrain(v[vs],p);
                f[vs] = myFunctionSimplex(v[vs],p,graphSize);
                constrain(v[vg],p);
                f[vg] = myFunctionSimplex(v[vg], p, graphSize);
                constrain(v[vh],p);
                f[vh] = myFunctionSimplex(v[vh], p, graphSize);
                count += 2;
            }
        }
        if (report) {
            printIteration(v, f, n, vs, rank);
        }
        //Test for convergence
        fsum = 0.0;
        for (int j = 0; j < n + 1; ++j) {
            fsum+=f[j];
        }
        favg = fsum/(n+1);
        s = 0.0;
        for (int j = 0; j < n + 1; ++j) {
            s+=pow((f[j]-favg),2.0)/(n);
        }
        s=sqrt(s);
        if(s<EPSILON)break;
    }
    //Find smallest value
    vs = findSmall(f,0,n);
    if(report){
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"The minimum was found at\n");
        for (int i = 0; i < n; ++i) {
            PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%f ",v[vs][i]);
            start[i] = v[vs][i];
        }
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"\nBest %f\n",f[vs]);
    }
    min = myFunctionSimplex(v[vs],p,graphSize);
    count++;
    if(verbose){
        printf("%d Function evaluations\n",count);
    }
    free(f);
    free(vr);
    free(ve);
    free(vc);
    free(vm);
    PetscScalar* result = malloc(sizeof(PetscScalar)*n);
    for (int i = 0; i < n; ++i) {
        result[i] = v[vs][i];
    }
    for (int i = 0; i < n + 1; ++i) {
        free(v[i]);
    }
    free(v);
    return result;
}

Vec probabilities(PetscScalar* parameters, int p, size_t graphSize, int rank){
    PetscErrorCode ierr;
    Vec initial = setInitialState(graphSize);
    for (int i = 0; i < p; ++i) {
        initial = genState(parameters[i],parameters[i+p],petOperatorC,petOperatorB,initial);
    }
    Vec conj;
    VecDuplicate(initial,&conj);
    VecCopy(initial,conj);
    VecConjugate(conj);
    VecPointwiseMult(conj,initial,conj);
    PetscScalar* output;
    VecGetArray(conj,&output);
    PetscInt size;
    VecGetSize(conj,&size);
    if(report){
        PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"Probabilities:\n");
        for (PetscInt i = 0; i < size; ++i) {
            PetscFPrintf(PETSC_COMM_WORLD,fout[rank],"%f\n",output[i]);
        }
    }
    VecDestroy(&initial);
    return conj;
}