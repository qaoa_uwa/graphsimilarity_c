# !/bin/bash
module swap PrgEnv-cray PrgEnv-gnu
module load gsl
module load slepc-complex
cc -O3 -dyanmic main.c -I$GSL_DIR/include -I$PETSC_DIR/include -I$SLEPC_DIR/include -L$GSL_DIR/lib -lgsl -L$PETSC_DIR/lib -lcraypetsc_gnu_complex -L$SLEPC_DIR/lib -lslepc -o main.x 


