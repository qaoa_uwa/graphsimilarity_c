#!/bin/bash

module load petsc-complex
module swap Prg-Env-cray Prg-Env-gnu
module load gsl
