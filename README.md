# README #

Welcome to the ReadMe, enjoy your stay.

### What is this repository for? ###

This respository contains a C implementation of the Quantum Approximate Optimisation Algorithm (QAOA) intended for both desktop and high-performance use.
The intention is to develop both a GSL and Petsc implementation, the current implementation is built using GSL.

Version: 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review `clap`
* Other guidelines

### Who do I talk to? ###

* Nicholas Pritchard 21726929@student.uwa.edu.au